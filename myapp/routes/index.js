var express = require('express');
var router = express.Router();

var generator = require('generate-password');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Generation',password : "" });
});

router.get('/generate', function (req, res, next) {
    var password = generator.generate({
        length: 10,
        numbers: true,
        symbols : true,
        uppercase : false
    });
   res.render('index',{ title: 'Generation', password : password})
});




module.exports = router;