var generator = require('generate-password');

/*TU*/
var expect = require("chai").expect;
var request = require("request");
describe("Example Mocha test suite", function() {
    describe("Example group 1", function() {
        var password = generator.generate({
            length: 10,
            numbers: true,
            symbols : true
        });

        it("should be a string", function() {
            expect(password).to.be.a('string');
        });

        it("should have all specifications", function() {
            expect(password).to.have.lengthOf(10);
            
        });
    }) ;
});